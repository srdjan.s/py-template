import uvicorn
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from core.config import config
from starlette.requests import Request
from starlette.responses import Response
from core.orm import create_session
from core import logg
from {{cookiecutter.app_name}} import beers

# Initialize Logging configuration from config yaml loader
logg.init()
log = logg.use('{{cookiecutter.app_name}}')

# FastAPI application setup
app = FastAPI()

app.include_router(
    beers.router,
    prefix='/beers',
    tags=['Beer']
)

app.add_middleware(CORSMiddleware,
                   allow_origins=['*'],
                   allow_methods=['*'],
                   allow_headers=['*'])


@app.middleware('http')
async def db_session_handler(request: Request, call_next):
    response = Response("Internal server error.", status_code=500)
    try:
        request.state.db = create_session()
        response = await call_next(request)
    except Exception as e:
        import traceback
        traceback.print_exc(50)
    finally:
        request.state.db.close()
    return response


@app.on_event('startup')
def startup_event():
    log.info("Starting app {{cookiecutter.app_name}}")


@app.get('/')
def get_index():
    return dict(version='0.1a')

if __name__ == '__main__':
    uvicorn.run("main:app", 
    	host='0.0.0.0', 
    	port=config.app.port, 
    	reload=config.app.hot_reload, 
    	debug=config.app.debug)


import json

from typing import List
from pydantic import BaseModel

from fastapi import APIRouter
from fastapi import File, Form, UploadFile
from fastapi import Depends, HTTPException
from fastapi import Query

from sqlalchemy.orm import Session
import core.orm.models as models
from api.dependencies import db


router = APIRouter()

class OutputStyle(BaseModel):
    id: int = None
    name: str = None

    class Config:
        orm_mode = True


class OutputBeer(BaseModel):
    id: int
    name: str
    abv: str
    ibu: str
    srm: str
    upc: str
    descript: str
    last_mod: str

    style: OutputStyle

    class Config:
        orm_mode = True


@router.get('/', summary="Get beers !", response_model=List[OutputBeer])
def list_categories(
        *,
        sess: Session = Depends(db)):
    """
    Returns the list of beers from around the WORLD !
    """
    ret = []
    beers = sess.query(models.Beer).all()

    ret = [OutputBeer.from_orm(beer) for beer in beers]

    return ret

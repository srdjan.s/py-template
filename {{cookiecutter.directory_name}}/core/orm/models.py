# coding: utf-8
from sqlalchemy import BigInteger, Boolean, CHAR, Column, DateTime, Date, ForeignKey, ForeignKeyConstraint, \
    Integer, String, Table, Text, UniqueConstraint, Numeric, text, Enum
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import extract
import datetime
from datetime import datetime as dt
import enum

Base = declarative_base()
metadata = Base.metadata

class Beer(Base):
    __tablename__ = 'beers'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    id_style = Column(ForeignKey('beerstyle.id'))

    # Data types ? pffft
    name = Column(Text)
    abv = Column(Text)
    ibu = Column(Text)
    srm = Column(Text)
    upc = Column(Text)
    descript = Column(Text)
    last_mod = Column(Text)

    style = relationship('BeerStyle', lazy='joined')


class BeerStyle(Base):
    __tablename__ = 'beerstyle'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    name = Column(Text)
